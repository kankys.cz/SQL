
**Smazání Dat v tabulce:**

```
DELETE FROM [Uzivatele];

```


**Smazání celé tabulky**

```
DROP TABLE [Uzivatele];

```

**Vymaže uživatele s ID 2**

```
DELETE FROM [Uzivatele] WHERE [Id] = 2;

```

**Vymaže vše v Tabulce!! Nutno používat podmínku WHERE**

```
DELETE FROM [Uzivatele];

```


**Editace dat v tabulce**

```
UPDATE [Uzivatele] SET [Prijmeni] = 'Dolejší', [PocetClanku] = [PocetClanku] + 1 WHERE [Id] = 1;

```
